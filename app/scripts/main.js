/* jshint devel:true */

/**
 * Ftt London: From time to time in London
 *
 * This webapp is made only with javascript, $, html5 and pure css3 to show some basic
 * skills about frontend development.
 *
 *  by César González
 */

(function(google, $){

    'use strict';


    var app = {

        settings : {
                center: new google.maps.LatLng(51.5061024,-0.1251163),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: true,
                zoom: 16,
                apiUrl : 'http://localhost:5000'
        },

        map : {},
        infoWindow : null,
        markers : [],

        init : function() {
            var app = this;
            app.map = new google.maps.Map(document.getElementById('map-container'), this.settings);
            app.infoWindow = new google.maps.InfoWindow();
            app.infoWindow.setOptions( { maxWidth: 340 } );
            app.setListeners();
        },

        setListeners : function () {

            var map = app.map;

            // Add listener for map drag event
            google.maps.event.addListener(map, 'drag', function() {
                map.getCenter();
                app.clearBottom();
            });

            // Add listener for zoom changes event
            google.maps.event.addListener(map, 'zoom_changed', function() {
                map.getCenter();
                app.clearBottom();
            });

            // Add listener for map ready
            google.maps.event.addListener(map, 'idle', function() {

                var bounds =  map.getBounds();

                app.clearBottom();

                var ne = [ bounds.getNorthEast().lat(), bounds.getNorthEast().lng() ];
                var sw = [ bounds.getSouthWest().lat(), bounds.getSouthWest().lng() ];

                app.getStops(ne, sw);

            });

        },

        getStops : function (ne, sw) {
            app.showLoading();

            $.ajax({
                type: 'GET',
                url: app.settings.apiUrl + '/bus-stops',
                dataType: 'jsonp',
                jsonp: 'callback',
                data: 'northEast=' + ne + '&southWest=' + sw,
                cache: true
            }).done(function (data) {
                app.setMarkers(data);
                app.hideLoading();
            }).fail(function (XHR, status, error) {
                console.log(error);
            });
        },

        setMarkers : function(data) {

            function attachMarker(marker) {


                // Attach listener by marker
                (function(marker) {
                    google.maps.event.addListener(marker, 'click', function() {

                        app.infoWindow.setContent( app.infoWindowContent( marker ) );
                        app.infoWindow.open( app.map, marker );

                        // Get Bus Stop details including arrivals. Using promises.
                        var promiseStopData = app.getStopData( marker.markerStop.id );
                        promiseStopData.done(function (data) {
                            $('.routeBtn').removeClass('off').addClass('on');
                            $('.routeBtn').mouseover(function(event) {
                                app.showArrivals(event.target.id, data);
                            });

                            app.hideLoading();

                        }).fail(function (XHR, status, error) {
                            console.log(error);
                            app.hideLoading();
                        });

                        // Clear bottom section when close infoWindow
                        google.maps.event.addListener(app.infoWindow, 'closeclick', function() {
                            app.clearBottom();
                        });

                    });

                    // Add hoover fx to markers on mouse over
                    google.maps.event.addListener(marker, 'mouseover', function() {
                        marker.setIcon('images/bus4.png');
                    });
                    // Remove hoover fx on mouse out
                    google.maps.event.addListener(marker, 'mouseout', function() {
                        marker.setIcon('images/bus5.png');
                    });

                    // Check if marker is already displayed then don't diplay it again
                    if ( !app.checkMarker( marker.markerStop.id ) ) {
                        // Display marker
                        marker.setMap( app.map );
                        // Add the new marker to our array of markers
                        app.markers.push( marker );
                    }

                })(marker);
            }

             // Looping through the markers array
            for (var i = 0; i < data.markers.length; i++) {
                // Creating a new marker
                var marker = new google.maps.Marker({
                    position: data.markers[i],
                    //map: app.map,
                    title: 'Stop: ' + data.markers[i].id,
                    icon: 'images/bus5.png',
                    markerStop: data.markers[i]
                });

                attachMarker(marker);
            }

            $('body').on('click', '.routeBtn', function(event){
                event.preventDefault();
            });


        },

        checkMarker : function (id) {
            var exist = $.map(app.markers, function(val) {
                return val.markerStop.id === id ? val : null;
            });

            if (exist.length === 0) {
                return false;
            } else {
                return true;
            }
        },

        cleanMarkers : function () {
            for (var i = 0; i < app.markers.length; i++) {
                app.markers[i].setMap( null );
            }

            app.markers.length = 0;
        },

        infoWindowContent : function ( marker ) {

            var routes = marker.markerStop.routes;
            var routesHtml = [];

            // Add routes labels
            for(var j=0; j < routes.length; j++){
                var route = routes[j];
                routesHtml.push( '<li><span class="routeBtn off" id="'+ route.id +'">' + route.name + '</span></li>' );
            }

            // Prepare infoWindow content
            var content = '<div id="content">'+
                '<h2>'+ marker.markerStop.name +'</h2>'+
                '<h3>'+ marker.markerStop.id + ( marker.markerStop.stopIndicator ? '[' + marker.markerStop.stopIndicator + ']' : '' ) + '</h3>'+
                '<div id="bodyContent">'+
                '<p>Towards: <b>' + ( marker.markerStop.towards ? marker.markerStop.towards : 'Not defined' ) + '</b></p>'+
                '<p>Bus Routes:</p>'+
                '<p><ul class="list-routes">'+ routesHtml.join('') +'</ul></p>'+
                '<p>Place the mouse pointer over any route to see arrival details.</p>'+
                '</div>'+
                '</div>';

            return content;

        },

        getStopData : function (id) {
            app.showLoading();

            var promise = $.ajax({
                type: 'GET',
                url: app.settings.apiUrl + '/bus-stops/' + id,
                dataType: 'jsonp',
                jsonp: 'callback',
                cache: true
            });

            return promise;
        },

        showArrivals : function (id, sData) {

            var arrivalsHtml = [];

            for(var i = 0; i< sData.arrivals.length; i++){
                var obj = sData.arrivals[i];

                if(obj.routeId === id){

                    arrivalsHtml.push('<ul class="arrival"><li>'+ obj.routeName + '</li>' );
                    arrivalsHtml.push('<li class="dest">'+ obj.destination + '</li>' );
                    arrivalsHtml.push('<li>'+ obj.scheduledTime + '</li>' );
                    arrivalsHtml.push('<li>'+ obj.estimatedWait + '</li>' );
                    arrivalsHtml.push('<li'+ ( obj.isCancelled ? ' style="background-color: red"' : '' ) + '>'+ ( obj.isCancelled ? '&#65794;' : '&#10003;' ) + '</li></ul>' );
                }

            }

            if( arrivalsHtml.length > 0 ) {
                arrivalsHtml = '<label class="arrivals_tit">Arrivals:</label>' + arrivalsHtml.join('');
                $('.bottom').empty().append( arrivalsHtml );
                $('.bottom').addClass('on');

                $('.arrival').each(function(i) {
                    $(this).delay((i++) * 100).fadeTo(200, 1);
                });

            } else {
                app.clearBottom();
                $('.bottom').html('<span class="noarrivals">No arrivals data at this moment for Stop: <span class="route">' + id +'</span></span>');

            }

        },

        clearBottom : function () {
            $('.bottom').empty().removeClass('on');
            $('.bottom').html('<span class="credits">por César González: </span>');
            $('.bottom').html('<span class="noarrivals">Please select a Bus Stop to see the routes then you can place the mouse pointer over one to see arrival details.</span>');

        },

        showLoading : function () {
            $('.loading').show();
        },

        hideLoading : function () {
            $('.loading').hide();
        }
    };


    $(function() {
        // check if there is map container
        var mapContainer = $('#map-container');
        if (mapContainer.length === 0) {
            console.log('Error! map container is not found !');
            return false;
        }

        app.init();
    });

})(google, jQuery);
