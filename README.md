From time to time in London (A test Frontend Application)
=====================================

> by César González
> email: g*******c@gmail.com

Dependencies
--------------

  * node
  * git
  * gulp
  * bower

Install
-----------

With Git and Node.js already installed:

Clone the repo:

```
#!shell

git clone https://caesarlabs-admin@bitbucket.org/caesarlabs-admin/ftt-london.git
```


If Gulp is not installed yet:

```
#!shell

npm install -g gulp
```


If Bower is not installed yet:

```
#!shell

npm install -g bower
```


Install dev dependencies:

```
#!shell

npm install
```


Install runtime dependencies:

```
#!shell

bower install
```


Run
------

* To run develoment server:

```
#!shell

gulp serve
```
 
* To create distributed version into folder:  **/dist**

```
#!shell

gulp build
```

***
That's all folks.